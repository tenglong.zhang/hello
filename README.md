# hello

This is a simple example project that demonstrates the use of Environment Variables to pass values to a Java application deployed using docker.

# Running the example using Docker

* Download the code
* Goto the project root and run `mvn package` to compile the source code
* Now run: `docker build -t hello:latest .` to build the Docker image
* Finally, run: `docker run hello:latest` to deploy your app

## Notes

You can change the name that is printed out by using the ``-e`` flag:

```
docker run -e NAME=Arthur hello:latest
```

# Running the example using docker-compose

Follow the same first two steps as above an then run:

```
docker-compose build
```

Followed by:
```
docker-compose up
```

## Notes

You can combine both steps into a single step:

```
docker-compose --force-recreate up
```
